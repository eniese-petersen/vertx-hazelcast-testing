package com.privacystar.test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class ThreadTwo implements Runnable {

    @Override
    public void run() {
        // Create cluster instance
        HazelcastInstance hcInstance = Hazelcast.newHazelcastInstance();
        BlockingQueue<String> queue = hcInstance.getQueue("hc_test");

        // check queue every 1 seconds
        String item;
        while(true) {
            try {
                item = queue.poll(20, TimeUnit.SECONDS);
            }
            catch (Exception e) {
                item = null;
            }
            if (item != null && !item.isEmpty())
                System.out.println("Received: " + item);
        }

    }
}

