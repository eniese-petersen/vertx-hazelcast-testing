package com.privacystar.test;

import io.vertx.core.eventbus.EventBus;

import java.util.Random;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class ThreadThree implements Runnable {
    private static  EventBus eventBus;

    public ThreadThree(EventBus eb) {
        eventBus = eb;
    }

    @Override
    public void run() {
        final long threadId = Thread.currentThread().getId();
        Random rnd = new Random();
        long runCnt = 0;

        while(true) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                // ignore
            }

            runCnt++;

            int bound = rnd.nextInt(5)+1;

            System.out.println("Run " + runCnt + " sending " + bound + " to EventBus on thread " + threadId);
            for(int x = 0; x < bound; x++)
                eventBus.send("eb-testing", "EventBus [" + threadId + "] [Run " + runCnt + "] test " + (x+1));
        }
    }
}
