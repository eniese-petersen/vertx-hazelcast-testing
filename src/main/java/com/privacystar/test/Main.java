package com.privacystar.test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();

        boolean useHCOnly = true;
        boolean useVertxOnly = false;
        boolean isReceiver = false;

        if (args.length == 1 && args[0].equals("r"))
            isReceiver = true;
        else if (args.length == 1) {
            useHCOnly = false;
            if (args[0].equals("vo"))
                useVertxOnly = true;
        }
        else if (args.length == 2 && args[0].startsWith("v") && args[1].equals("r")) {
            useHCOnly = false;
            isReceiver = true;

            if (args[0].equals("vo"))
                useVertxOnly = true;
        }

        if (useHCOnly) {
            if (!isReceiver)
                executor.execute(new ThreadOne());
            else
                executor.execute(new ThreadTwo());

            executor.shutdown();
            executor.awaitTermination(0, TimeUnit.SECONDS);
        }
        else {
            VertxOptions vxOptions;

            if (useVertxOnly) {
                vxOptions = new VertxOptions().setClustered(true);
            } else {
                HazelcastInstance hcInstance = Hazelcast.newHazelcastInstance();
                ClusterManager clusterMgr = new HazelcastClusterManager(hcInstance);
                vxOptions = new VertxOptions().setClusterManager(clusterMgr).setClustered(true);
            }

            final boolean checkReceiver = isReceiver;

            Vertx.clusteredVertx(vxOptions, result -> {
                if (result.succeeded()) {
                    System.out.println("Vertx Cluster started.");

                    DeploymentOptions dOptions = new DeploymentOptions().setWorker(true);

                    if (checkReceiver) {
                        result.result().deployVerticle("com.privacystar.test.VerticleTwo", dOptions, dResult -> {
                            if (dResult.succeeded()) {
                                System.out.println("Verticle receiver running.");
                            } else {
                                System.out.println("Failed starting Verticle receiver. Stopping Vertx Cluster.");
                                result.result().close();
                            }
                        });
                    } else {
                        result.result().deployVerticle("com.privacystar.test.VerticleOne", dOptions, dResult -> {
                            if (dResult.succeeded()) {
                                System.out.println("Verticle sender running.");
                            } else {
                                System.out.println("Failed starting Verticle sender. Stopping Vertx Cluster.");
                                result.result().close();
                            }
                        });
                    }
                } else {
                    System.out.println("Failed to start Vertx Cluster");
                }
            });

        }
    }
}
