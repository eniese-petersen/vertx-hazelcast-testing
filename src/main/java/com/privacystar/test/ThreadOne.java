package com.privacystar.test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Queue;
import java.util.Random;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class ThreadOne implements Runnable {

    @Override
    public void run() {
        final long threadId = Thread.currentThread().getId();
        Random rnd = new Random();
        // Create cluster instance
        HazelcastInstance hcInstance = Hazelcast.newHazelcastInstance();
        Queue<String> queue = hcInstance.getQueue("hc_test");

        long runCnt = 0;
        while(true) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                // ignore
            }

            runCnt++;
            int bound = rnd.nextInt(5)+1;

            System.out.println("Run " + runCnt + " sending " + bound + " to queue on thread " + threadId);
            for(int x = 0; x < bound; x++)
                queue.offer("[" + threadId + "] [Run " + runCnt + "] test " + (x+1));
        }
    }
}
