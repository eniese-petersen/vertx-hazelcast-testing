package com.privacystar.test;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.impl.codecs.StringMessageCodec;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class VerticleTwo extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        try {
            EventBus eventBus = this.getVertx().eventBus();

            eventBus.consumer("eb-testing", new Handler<Message<String>>() {
                @Override
                public void handle(Message<String> msg) {
                    System.out.println("Received: " + msg.body());
                }
            });

            startFuture.complete();
        } catch (Exception e) {
            startFuture.fail(e.toString());
        }
    }
}