package com.privacystar.test;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;

/**
 * Created by eniese-petersen on 1/19/2016.
 */
public class VerticleOne extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        try {
            EventBus eventBus = this.getVertx().eventBus();

            Thread thread = new Thread(new ThreadThree(eventBus));
            thread.start();
            startFuture.complete();
        } catch (Exception e) {
            startFuture.fail(e.toString());
        }
    }
}
